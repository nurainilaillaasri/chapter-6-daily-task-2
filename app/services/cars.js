const { cars } = require("../models");

module.exports = {
    getAllCar(a) {
        const car = cars.findAll(a)
        return car 
    },
    createCar(id_customer, car_name, price, type){
        const car = cars.create({
            id_customer, car_name, price, type
        });
        return car;
    },
    updateCar(car, carUpdate){
        return car.update(carUpdate)
    },
    deleteCar(car){
        return car.destroy()
    },
    findKey(findIdCar){
        return cars.findByPk(findIdCar)
    }
}
