const { customers } = require("../models");

module.exports = {
    getAllCustomer(a) {
        const customer = customers.findAll(a)
        return customer 
    },
    createCustomer(customer_name, address){
        const customer = customers.create({
            customer_name, address
        });
        return customer;
    },
    updateCustomer(customer, customerUpdate){
        return customer.update(customerUpdate)
    },
    deleteCustomer(customer){
        return customer.destroy()
    },
    findByPk(findIdCustomer){
        return customers.findByPk(findIdCustomer)
    }
}
