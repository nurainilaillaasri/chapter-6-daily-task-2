/**
 * @file contains request handler of customers resource
 * @author Fikri Rahmat Nurhidayat
 */
const { customers, cars } = require("../../../models");
const customersService = require("../../../services/customers");

module.exports = {
    list(req, res) {
        customersService.getAllCustomer({
            include: {
                model: cars,
            }
        })
        .then((customers) => {
            res.status(200).json({
                status: "OK",
                data: {
                    customers,
                },
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },

    create(req, res) {
        const { customer_name, address } = req.body;
        customersService.createCustomer(customer_name, address)
        .then((customers) => {
            res.status(201).json({
                status: "OK",
                data: customers,
            });
        })
        .catch((err) => {
            res.status(401).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },

    update(req, res) {
        const customers = req.customers;
        customersService.updateCustomer(customers, req.body)
            .then(() => {
                res.status(200).json({
                    status: "OK",
                    data: customers,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    show(req, res) {
        const customers = req.customers;

        res.status(200).json({
            status: "OK",
            data: customers,
        });
    },

    destroy(req, res) {
        customersService.deleteCustomer(req.customers)
            .then(() => {
                res.status(204).end();
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    setCustomers(req, res, next) {
        customersService.findByPk(req.params.id)
            .then((customers) => {
                if (!customers) {
                    res.status(404).json({
                        status: "FAIL",
                        message: "Customer not found!",
                    });

                    return;
                }

                req.customers = customers;
                next()
            })
            .catch((err) => {
                res.status(404).json({
                    status: "FAIL",
                    message: "Customer not found!",
                });
            });
    },
};
