const { cars, customers } = require("../../../models");
const carsService = require("../../../services/cars");

module.exports = {
    list(req, res) {
        carsService.getAllCar({
            include: {
                model: customers,
            }
        })
            .then((cars) => {
                res.status(200).json({
                    status: "OK",
                    data: {
                        cars,
                    },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },


    create(req, res) {
        const { id_customer, car_name, price, type } = req.body;
        carsService.createCar(id_customer, car_name, price, type)
            .then((cars) => {
                res.status(201).json({
                    status: "OK",
                    data: cars,
                });
            })
            .catch((err) => {
                res.status(401).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    update(req, res) {
        const cars = req.cars;
        carsService.updateCar(cars, req.body)
            .then(() => {
                res.status(200).json({
                    status: "OK",
                    data: cars,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    show(req, res) {
        const cars = req.cars;

        res.status(200).json({
            status: "OK",
            data: cars,
        });
    },

    destroy(req, res) {
        carsService.deleteCar(req.cars)
            .then(() => {
                res.status(204).end();
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    setCars(req, res, next) {
        carsService.findKey(req.params.id)
            .then((cars) => {
                if (!cars) {
                    res.status(404).json({
                        status: "FAIL",
                        message: "Post not found!",
                    });

                    return;
                }

                req.cars = cars;
                next()
            })
            .catch((err) => {
                res.status(404).json({
                    status: "FAIL",
                    message: "Post not found!",
                });
            });
    },
};
