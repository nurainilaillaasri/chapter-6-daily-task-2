/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const post = require("./post");
const customers = require("./customers");
const cars = require("./cars");

module.exports = {
  post,
  customers,
  cars,
};
