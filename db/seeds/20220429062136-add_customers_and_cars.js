'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
        */
        await queryInterface.bulkInsert('customers', [{
            customer_name: 'Lailla', 
            address: 'Karawang',
            createdAt: new Date(),
            updatedAt: new Date()
        }]);

        await queryInterface.bulkInsert('cars', [{
            id_customer: 1, 
            car_name: 'Toyota', 
            price: 100000000, 
            type: 'Small',
            createdAt: new Date(),
            updatedAt: new Date()
        }]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('customers', null, {});
        await queryInterface.bulkDelete('cars', null, {});
    }
};
